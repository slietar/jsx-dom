/**
 * jsx-dom
 */


const Fragment = Symbol();


function createElement(tag, attributes, ...children) {
  if (tag === Fragment) {
    return children;
  }

  let isSvgElement = [
    'circle', 'ellipse', 'g', 'image', 'line', 'path', 'polygon', 'polyline', 'rect', 'svg', 'text', 'use'
  ].indexOf(tag) >= 0;

  let element = isSvgElement
    ? document.createElementNS('http://www.w3.org/2000/svg', tag)
    : document.createElement(tag);

  let refsGlobal = {};
  let refsLocal = { self: element };
  let childrenRefsLocal = {};

  if (attributes) {
    for (let [key, value] of Object.entries(attributes)) {
      if (key === 'ref') {
        continue; // handled later on
      } else if (key.startsWith('on')) {
        element.addEventListener(key.substring(2), value);
      } else {
        element.setAttribute(key, value);
      }
    }
  }

  for (let child of children.flat()) {
    if (child !== null && child !== void 0) {
      if (typeof child === 'number' || typeof child === 'string') {
        element.appendChild(document.createTextNode(child.toString()));
      } else if (typeof child === 'object' && child instanceof Element) {
        element.appendChild(child);
      } else {
        let { global: childRefsGlobal, local: childRefsLocal } = child;

        element.appendChild(childRefsLocal.self);
        refsGlobal = { ...refsGlobal, ...childRefsGlobal };
        childrenRefsLocal = { ...childrenRefsLocal, ...childRefsLocal };
      }
    }
  }

  if (attributes && attributes.ref) {
    let value = attributes.ref;

    if (value.startsWith('.')) {
      refsLocal[value.substring(1)] = createReferences({ ...childrenRefsLocal, self: element });
    } else {
      refsGlobal[value] = createReferences({ ...childrenRefsLocal, self: element });
    }
  } else {
    refsLocal = { ...childrenRefsLocal, ...refsLocal };
  }

  return { global: refsGlobal, local: refsLocal };
}


function createReferences(_refs) {
  return new Proxy(_refs, {
    get: (obj, key) => {
      return obj[key];
    },
    set: (obj, key, value) => {
      if (!(key in obj)) {
        throw new Error(`Missing reference '${key}'`);
      }

      let element = obj[key].self;

      let appendChild = (value) => {
        if (typeof value === 'number' || typeof value === 'string') {
          element.appendChild(document.createTextNode(value.toString()));
        } else if (typeof value === 'object' && value instanceof Element) {
          element.appendChild(value);
        } else if (typeof value === 'object' && value.global && value.local) {
          element.appendChild(value.local.self);
        } else {
          return false;
        }

        return true;
      };

      if (Array.isArray(value)) {
        element.innerHTML = '';
        obj[key] = createReferences({ self: element });
        return value.every(appendChild);
      } else if (typeof value === 'object' && value instanceof Element) {
        element.parentElement.replaceChild(value, element);
        obj[key] = createReferences({ self: value });
      } else if (typeof value === 'object' && value.global && value.local) {
        element.parentElement.replaceChild(value.local.self, element);
        obj[key] = createReferences(value.local);
      } else {
        element.innerHTML = '';
        obj[key] = createReferences({ self: element });
        return appendChild(value);
      }

      return true;
    }
  });
}


function getReferences(tree) {
  return createReferences({ ...tree.local, ...tree.global });
}


exports.Fragment = Fragment;
exports.createElement = createElement;
exports.createReferences = createReferences;
exports.getReferences = getReferences;

