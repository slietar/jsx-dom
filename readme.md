# jsx-dom


## example

```js
/** @jsx createElement */
/** @jsxFrag Fragment */

import { Fragment, createElement, getReferences } from 'jsx-dom';

let tree = (
  <div>
    <h1>Title</h1>
    <p>Text</p>
  </div>
);

document.body.appendChild(getReferences(tree).self);
```


## features

### references

```js
let tree = (
  <div>
    <h1 ref="title">Title</h1>
  </div>
);

let refs = getReferences(tree);

// replace children with a single text node
refs.title = 'Updated title';

// replace element (reference will be kept)
// for now, global references in children are ignored
refs.title = <h1>Updated title</h1>;

// replace children
// for now, all references in children are ignored
refs.title = [<strong>Updated</strong>, ' title'];
refs.title = <><strong>Updated</strong> title</>;

// retreive element
let h1 = refs.title.self;
```

### subreferences

```js
let tree = (
  <ul>
    <li ref="item1">
      <div>
        <!-- refers to the closest parent reference -->
        <div ref=".title">Title item 1</div>
      </div>
    </li>
    <li ref="item2">
      <div>
        <div ref=".title">Title item 2</div>
      </div>
    </li>
  </ul>
);

let refs = getReferences(tree);

// update subreference
refs.item1.title = 'Updated title item 1';
refs.item2.title = 'Updated title item 2';

// retreive subreference element
let title1 = refs.item1.title.self;
```

### event listeners

```js
let handler = (event) => {
  // ...
};

let tree = (
  <a href="#" onclick={handler}>Link</a>
);
```

